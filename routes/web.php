<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',  'IndexController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/music', 'MusicController@index')->name('music');
Route::post('/music', 'MusicController@search')->name('music_search');

Route::post('/dislike', 'MusicController@search')->name('music_search');
Route::post('/like', 'MusicController@search')->name('music_search');
