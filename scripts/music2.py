#!/usr/bin/env python
import requests
import json
import sys
from pprint import pprint

class Music(object):

	def __init__(self, search):

		self.search = search
		who = sys.argv[1]

		title = self.search
		print(title)
		print("------")


	def deezer(self):

		search = self.search

		r = requests.get('http://api.deezer.com/search?q={}&limit={}&output={}'.format(search, 1, json))
		data = r.json()

		for i in data['data']:
			return i['link']


	def spotify(self):

		search = self.search

		r = requests.get("http://api.spotify.com/v1/search?q={}&limit={}&type=track".format(search, 1), verify=True)
		data = r.json()

		for i in data['tracks']['items']:
			return '\n' + i['external_urls']['spotify'] + '\n\n' + i['preview_url'] + '\n' + i['album']['images'][1]['url'] + '\n'


	def itunes(self):

		search = self.search

		r = requests.get("http://itunes.apple.com/search?term={}&country=FR&entity=song&limit=1".format(search), verify=True)
		data = r.json()

		for i in data['results']:
			return i['trackViewUrl']



if __name__ == '__main__':
        m = Music(sys.argv[1])
        print(m.deezer())
        print(m.spotify())
        print(m.itunes())
