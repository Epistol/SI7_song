<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
	private $recherche;

	public function __construct( array $attributes = [] ) {
	    parent::__construct( $attributes );

	    $this->recherche = $attributes[0];

    }

    static function deezer($recherche){

		$retour = file_get_contents('http://api.deezer.com/search?q='.urlencode($recherche).'&limit=6&output=json');
		$jason = json_decode($retour);
		return $jason->data;

    }

	static function itunes($recherche){

		$retour = file_get_contents('http://itunes.apple.com/search?term='.urlencode($recherche).'&country=FR&entity=song&limit=6');
		$jason = json_decode($retour);
		return $jason->results;

	}

	static function spotify($recherche){

		$retour = file_get_contents('https://api.spotify.com/v1/search?q='.urlencode($recherche).'&type=track&offset=0&limit=6');
		$jason = json_decode($retour);
		return $jason->tracks;

	}


}
