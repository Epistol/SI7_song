
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app',
    ready : function(){
        var socket = io('http://localhost:3000');
        socket.on('test-channel:UserRegistered', function(message){
            console.log(message);
            self.users.push(message.user);
        })
    }


});


$(function () {
    $('.likeing').click(function () { likeFunction(this); });
/*    $('.dislike').click(function () { dislikeFunction(this);});*/
});




function likeFunction(caller) {
    var postId = caller.parentElement.getAttribute('postid');

    if ($(caller).hasClass('red')) {
        $(caller).removeClass('red');
        $.ajax({
            type: "POST",
            url: "dislike",
            data: 'Action=DISLIKE&PostID=' + postId,
            success: function () {}
        });

    } else {
        $(caller).addClass('red');
        $.ajax({
            type: "POST",
            url: "like",
            data: 'Action=LIKE&PostID=' + postId,
            success: function () {}
        });

    }




}
function dislikeFunction(caller) {
    var postId = caller.parentElement.getAttribute('postid');
    $.ajax({
        type: "POST",
        url: "rate.php",
        data: 'Action=DISLIKE&PostID=' + postId,
        success: function () {}
    });
}
