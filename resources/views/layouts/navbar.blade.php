<nav class="navbar navbar-default navbar-static-top">

        <!-- Collapsed Hamburger -->
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>



    <div class="columns">
        <div class="column is-2">
           Icon user
        </div>
        <div class="column is-2">
            <!-- Branding Image -->
            <a class="" href="{{ url('/') }}">
                <img src="{{ asset('img/tempo.png') }}" style="height: 50px"/>
            </a>
        </div>
        <div class="column is-half"><form id="form_search" action="{{ route('music_search') }}" method="POST" style="">{{ csrf_field() }}
                    <input class="input" name="recherche" type="text" placeholder="Chercher une musique, un artiste, etc ...">
            </form>
        </div>
        <div class="column">
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>



</nav>
