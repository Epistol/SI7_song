<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://use.fontawesome.com/403c56d95d.js"></script>
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
       @include('layouts.navbar')

        <div class="columns">
            <div class="column is-2">
               @include('layouts.aside')
            </div>
            <div class="column">
                @yield('content')
            </div>
        </div>



    </div>

    @include('layouts.footer')

    <!-- Scripts -->
    <script src="{{ asset('js/socket.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>



</body>
</html>
