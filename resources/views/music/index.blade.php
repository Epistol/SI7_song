@extends('layouts.app')

@section('content')

    <p>Résultat pour : {{$recherche}}</p>


        <example></example>



    <section class="section deezer">
        <img src="{{asset('img/DZ_Logo_CMYK.png')}}" style="height: 1rem"/>

        <div class="container">
            <div class="columns">

                @foreach ($test as $music)

                    <div class="column">


                        <div class="card">
                            <div class="card-image">
                                <figure class="image is-4by3">
                                    <img src="{{$music->album->cover_medium}}" alt="Image">
                                </figure>
                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-content">
                                        <p class="">{{$music->title}}</p>
                                        <p class="">{{$music->artist->name}}</p>
                                    </div>
                                </div>

                                <div class="content">
                                    <div class="post" postid="{{$music->id}}">
                                        <a class="likeing"  class="like">
                                            <span class="icon is-small"><i class="fa fa-heart"></i></span>
                                        </a>

                                    </div>
                                    <audio controls>
                                        <source src="{{$music->preview}}" type="audio/mpeg">
                                        Your browser does not support the audio element.
                                    </audio>
                                </div>
                            </div>
                        </div>


                    </div>
                @endforeach
            </div>

        </div>


    </section>
    <section class="section spotify">
        <img src="{{asset('img/Spotify_Logo_RGB_Green.png')}}" style="height: 2.5rem"/>
        <div class="container">
            <div class="columns">
                @foreach ($spotify->items as $music)
                    <div class="column">
                        <div class="card">
                            <div class="card-image">
                                <figure class="image is-4by3">
                                  <img src="{{$music->album->images[0]->url}}" alt="Image">
                                </figure>
                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-content">
                                        <p class="">{{$music->name}}</p>

                                       <p class="">{{$music->artists[0]->name}}</p>
                                    </div>
                                </div>

                                <div class="content">
                                    <a class="">
                                        <span class="icon is-small"><i class="fa fa-heart"></i></span>
                                    </a>
                                    <audio controls>
                                        <source src="{{$music->preview_url}}" type="audio/mpeg">
                                        Your browser does not support the audio element.
                                    </audio>
                                </div>
                            </div>
                        </div>


                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="section itunes">
        <img src="{{asset('img/get_itune.svg')}}" style="height: 2.5rem"/>

        <div class="container">
            <div class="columns">

                @foreach ($itunes as $music)

                    <div class="column">


                        <div class="card">
                            <div class="card-image">
                                <figure class="image is-4by3">
                                    <img src="{{$music->artworkUrl100}}" alt="Image">
                                </figure>
                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-content">
                                        <p class="">{{$music->trackName}}</p>
                                        <p class="">{{$music->artistName}}</p>
                                    </div>
                                </div>

                                <div class="content">
                                    <audio controls>
                                        <source src="{{$music->previewUrl}}" type="audio/x-m4a">
                                        Your browser does not support the audio element.
                                    </audio>
                                </div>
                            </div>
                        </div>


                    </div>
                @endforeach
            </div>

        </div>


    </section>

@endsection

