@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="content">
            <form id="logout-form" action="{{ route('music_search') }}" method="POST" style="">
                {{ csrf_field() }}

                <div class="field">
                    <label class="label">Nom de la musique</label>
                    <p class="control">
                        <input class="input" name="recherche" type="text" placeholder="Text input">
                    </p>
                </div>

                <div class="field is-grouped">
                    <p class="control">
                        <button class="button is-primary">Envoyer</button>
                    </p>
                </div>
            </form>
        </div>


@endsection
